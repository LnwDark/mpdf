
<!doctype html>
<html lang="en">
<head>

</head>
<body>
<?php $logo = 'ischool.png';
$data=[
    'Receipt'=>'ใบกำกับภาษี / ใบเสร็จรับเงิน',
    'No_Bill'=>'BILL123004',
    'Date_Bill'=>date('d/m/Y'),
    'Seller_Bill'=>'บัญชี  ซื่อสัตย์',
    'Date_duly'=>date('d/m/Y'),
    'Company_name'=>'เนกเจน แอนด์ เน็ทออพ จํากัด',
    'Customer_name'=>'โรงเรียนเตรียมอุดมศึกษาพัฒนาการ อุบลราชธานี',
    'Company_Address'=>'เลขที่1558/41 ถนน บางนา-ตราด แขวงบางนา เขตบางนา กรุงเทพฯ 10260 โทร. 021014822 เลขประจําตัวผูเสียภาษี0105559127204 (สํานักงานใหญ่)',
    'Customer_Address'=>'ถนนคลังอาวุธ ตําบลขามใหญ่ อําเภอเมือง จังหวัดอุบลราชธานี 34000
                     เลขประจําตัวผู้เสียภาษี 099400092539 (สํานักงานใหญ่)',
];


?>
<img src="mpdf/img/Color.png"  width="80px" class="lor">
<div class="pagenum"><?= $page==1 ?'1':'2'; ?></div>
<div style="margin-top: -40px; padding-left: 20px;">
    <img src="mpdf/img/logo/<?= $logo ?>" width="150"  >
</div>
<div class="text1">ใบกำกับภาษี / ใบเสร็จรับเงิน</div>
<div  class="text2"><?= $page==1 ?'ต้นฉบับ':'สำเนา'; ?></div>
<div class="row">
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-10">
                <div ><?=$data['Company_name'];?></div>
                <div>
                    <?php
                    $datas =$data['Company_Address'];;
                    $br = "<br>";
                    $tel= strpos($datas,"โทร");
                    $data = substr_replace($datas, $br, $tel, 0);
                    $tex = strpos($datas,"เลขประจําตัว");
                    $data = substr_replace($datas, $br, $tex, 0);
                    echo $data;
                    ?>
                </div>
                <div style="font-weight: bold">ลูกค้า</div>

            </div>
        </div>
    </div>
    <div id="up-triangle" class="up-triangle"></div>
    <div class="col-xs-5">
        <div style="border: 1px solid #ddd; padding-right: 60px; padding-top: 10px;border-bottom: none;" ></div>
        <table width="100%">
            <tbody>
            <tr>
                <td style="padding-left: 50px; font-weight:bold;" >เลขที่</td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 50px;font-weight:bold;">วันที่</td>
                <td class="text-left"></td>
            </tr>
            <tr>
                <td style="padding-left: 50px;font-weight:bold;">ครบกำหนด</td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 50px;font-weight:bold;">อ้างอิง</td>
                <td></td>
            </tr>
            </tbody>
        </table>
        <div style="border: 1px solid #ddd;"></div>
    </div>
</div>

<div class="col-xs-12">
    <table class="tables">
        <thead>
        <tr>
            <th width="10%" class="text-center">#</th>
            <th width="50%" class="text-center">รายละเอียด</th>
            <th width="20%" class="text-right">จำนวน</th>
            <th width="20%" class="text-right">ราคาต่อหน่วย</th>
            <th width="10%" class="text-right">ยอดรวม</th>
        </tr>
        </thead>
        <tbody>
        <?php $data = [
            ['id'=>'1','name'=>'หาย','price'=>'100','amount'=>'9'],
            ['id'=>'2','name'=>'ชำรุด','price'=>'100','amount'=>'10'],
            ['id'=>'3','name'=>'ชำรุด','price'=>'100','amount'=>'10']
        ];?>
        <?php
        $Totaling=0;
        ?>
        <?php foreach ($data as $model):?>
            <tr>
                <td class="text-center" ><?=$model['id']?></td>
                <td><?=$model['name']?></td>
                <td class="text-right"><?=$model['amount']?></td>
                <td class="text-right"><?=number_format($model['price'],2)?></td>
                <td class="text-right"><?= number_format($sum=$model['amount']*=$model['price'],2)?></td>
                <?php  $Totaling +=$sum?>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>

<div class="col-xs-12" style="margin-top: 10px;">
    <div class="footersum"> รวมทั้งสิ้น</div>
    <div class="totalprice"><?=number_format($Totaling,2)?> บาท</div>
    <div class="footersum"> ภาษีมูลค่าเพิ่ม 7 % </div>
    <div class="totalprice"><?=number_format($Totaling*0.07,2)?> บาท</div>
    <div class="footersum"> รวมจำนวนเป็นเงิน </div>
    <div class="totalprice"><?=number_format($Totaling,2)?> บาท</div>
    <div class="asone"><?=FormatBaht($Totaling)?></div>
</div>
<div class="linefooter"></div>

</body>
</html>
