
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>

<table class="auto" width="100%" >
    <thead>
    <tr>
        <th class="text-center " width="5%">ลำดับ</th>
        <th class="text-center" width="10%">วันที่</th>
        <th class="text-left " width="10%">เลขที่เอกสาร</th>
        <th class="text-center " width="30%" >ชื่อผู้ซื้อสินค้า/ ผู้รับบริการสินค้า</th>
        <th class="text-center" width="15%">เลขประจำตัวผู้เสียภาษี</th>
        <th class="text-center " width="15%">สาขา</th>
        <th class="text-right" width="20%">มูลค่าสินค้า / บริการ</th>
        <th class="text-right" width="15%">จำนวนเงินภาษี</th>
    </tr>
    </thead>
    <tbody>
    <?php
     for ($i=$start;$i <= $end;$i++):
    ?>
    <tr>
        <td class="text-center"><?=$model[$i]['no'] ?></td>
        <td class="text-center">2016/11/03</td>
        <td class="text-left">2016072101222</td>
        <td class="text-left content-row" >จ่ายเงินสดให้กับบัริษัท เจเมกจำกัด</td>
        <td class="text-center">0105559055912</td>
        <td class="text-center content-row">สานักงานใหญ</td>
        <td class="text-right">10,000.00</td>
        <td class="text-right">700.00</td>
    </tr>
    <?php endfor;?>

    </tbody>
</table>



</body>
</html>
