<?php $logo = 'ischool.png';
$data=[
    'Receipt'=>'ใบกำกับภาษี / ใบเสร็จรับเงิน',
    'No_Bill'=>'BILL123004',
    'Date_Bill'=>date('d/m/Y'),
    'Seller_Bill'=>'บัญชี  ซื่อสัตย์',
    'Date_duly'=>date('d/m/Y'),
    'Company_name'=>'เนกเจน แอนด์ เน็ทออพ จํากัด',
    'Customer_name'=>'โรงเรียนเตรียมอุดมศึกษาพัฒนาการ อุบลราชธานี',
    'Company_Address'=>'เลขที่1558/41 ถนน บางนา-ตราด แขวงบางนา เขตบางนา กรุงเทพฯ 10260 โทร. 021014822 เลขประจําตัวผูเสียภาษี0105559127204 (สํานักงานใหญ่)',
    'Customer_Address'=>'ถนนคลังอาวุธ ตําบลขามใหญ่ อําเภอเมือง จังหวัดอุบลราชธานี 34000
                     เลขประจําตัวผู้เสียภาษี 099400092539 (สํานักงานใหญ่)',
];


?>
<img src="mpdf/img/Color.png"  width="80px" class="lor">
<div class="pagenum">{PAGENO}</div>
<div style="margin-top: -40px; padding-left: 20px;">
    <img src="mpdf/img/logo/<?= $logo ?>" width="150"  >
</div>
<div class="text1">ใบกำกับภาษี / ใบเสร็จรับเงิน</div>
<div  class="text2">ต้นฉบับ</div>
<div class="row">
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-10">
                <div style="font-size: 16px"><?=$data['Company_name'];?></div>
                <div><?php
                    $datas =$data['Company_Address'];;
                    $br = "<br>";
                    $tel= strpos($datas,"โทร");
                    $data = substr_replace($datas, $br, $tel, 0);
                    $tex = strpos($datas,"เลขประจําตัว");
                    $data = substr_replace($datas, $br, $tex, 0);
                    echo $data;
                    ?></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <div id="up-triangle" class="up-triangle"></div>
    <div class="col-xs-5">
        <div style="border: 1px solid #ddd; padding-right: 60px; padding-top: 10px;border-bottom: none;" ></div>
        <table width="100%">
            <tbody>
            <tr>
                <td style="padding-left: 50px; font-weight:bold;" >เลขที่</td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 50px;font-weight:bold;">วันที่</td>
                <td class="text-left"></td>
            </tr>
            <tr>
                <td style="padding-left: 50px;font-weight:bold;">ครบกำหนด</td>
                <td></td>
            </tr>
            </tbody>
        </table>
        <div style="border: 1px solid #ddd;"></div>
    </div>
</div>
