<?php
$logo = 'ischoolsmall.jpg';
 $data=[
     'Receipt'=>'ใบกำกับภาษี / ใบเสร็จรับเงิน',
     'No_Bill'=>'BILL123004',
     'Date_Bill'=>date('d/m/Y'),
     'Seller_Bill'=>'บัญชี  ซื่อสัตย์',
     'Date_duly'=>date('d/m/Y'),
     'Company_name'=>'เนกเจน แอนด์ เน็ทออพ จํากัด',
     'Customer_name'=>'โรงเรียนเตรียมอุดมศึกษาพัฒนาการ อุบลราชธานี',
     'Company_Address'=>'เลขที่1558/41 ถนน บางนา-ตราด <br>
                  แขวงบางนา เขตบางนา กรุงเทพฯ 10260<br>
                  โทร. 021014822 <br>
                  เลขประจําตัวผูเสียภาษี0105559127204 (สํานักงานใหญ่)',
     'Customer_Address'=>'ถนนคลังอาวุธ ตําบลขามใหญ่ อําเภอเมือง จังหวัดอุบลราชธานี 34000<br>
                     เลขประจําตัวผู้เสียภาษี 099400092539 (สํานักงานใหญ่)',

 ];

?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<style>
th
</style>
</head>
<body>


<img src="logo/<?= $logo ?>" width="150" class="text-left">
<p class="" style="margin-left: 450px; margin-top: -40px;font-size: 20px;"><?= $data['Receipt'] ?></p>
<p class="" style="margin-left:540px; font-size: 20px;"><?= $page==1 ?'ต้นฉบับ':'สำเนา'; ?></p>
<div class="row">
    <div class="col-xs-7">
        <div class="row">
            <div class="col-xs-10">
                <b style="font-size: 16px"><?=$data['Company_name']?></b><br>
                <p><?= $data['Company_Address']?></p>

                <b><?=$data['Customer_name']?></b>
                <p><?= $data['Customer_Address']?></p>
            </div>
        </div>
    </div>
    <div id="up-triangle" class="up-triangle"></div>
    <div class="col-xs-4">
        <div style="border: 1px solid #ddd;"></div>
        <table width="100%">
            <tbody>
            <tr>
                <th>เลขที่</th>
                <td><?=$data['No_Bill']?></td>
            </tr>
            <tr>
                <th>วันที่</th>
                <td><?= $data['Date_Bill']?></td>
            </tr>
            <tr>
                <th>ครบกำหนด</th>
                <td><?= $data['Date_duly'] ?></td>
            </tr>
            <tr>
                <th>ผู้ขาย</th>
                <td><?= $data['Seller_Bill'] ?></td>
            </tr>
            </tbody>
        </table>
        <div style="border: 1px solid #ddd;"></div>
    </div>
</div>
<table class="table table-bordered" width="100%">
    <thead>
    <tr>
        <th class="col-xs-1 text-center">#</th>
        <th class="col-xs-6 text-center">รายละเอียด</th>
        <th class="col-xs-2 text-center">จำนวน</th>
        <th class="col-xs-2 text-center">ราคาต่อหน่วย</th>
        <th class="col-xs-2 text-center">ยอดรวม</th>
    </tr>
    </thead>
    <tbody>
    <?php $data = [
        ['id'=>'1','name'=>'หาย','price'=>'100','amount'=>'9'],
        ['id'=>'2','name'=>'ชำรุด','price'=>'100','amount'=>'10']
    ];?>
    <?php
    $Totaling=0;
    ?>
    <?php foreach ($data as $model):?>
    <tr>
        <td class="text-center" ><?=$model['id']?></td>
        <td><?=$model['name']?></td>
        <td class="text-right"><?=$model['amount']?></td>
        <td class="text-right"><?=$model['price']?></td>
        <td class="text-right"><?=$sum=$model['amount']*=$model['price']?></td>
        <?php  $Totaling +=$sum?>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
<div class="row">
    <div class="col-xs-8 col-xs-offset-8">
        <table  class="table" width="100%">
            <tr>
                <th class="text-right" >รวมเป็นเงิน</th>
                <td class="text-right">&nbsp;&nbsp;<?=number_format($Totaling, 2, '.', ',');?>&nbsp;บาท</td>
            </tr>

            <tr>
                <th class="text-right" >จำนวนเงินรวมทั้งสิ้น</th>
                <td class="text-right">&nbsp;&nbsp;<?=number_format($Totaling, 2, '.', ',');?>&nbsp;บาท</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <th class="text-right" >ภาษีมูลค่าเพิ่ม 7%</th>
                <td class="text-right">&nbsp;&nbsp;<?=number_format($vat=$Totaling*0.07, 2, '.', ',');?>&nbsp;บาท</td>
            </tr>
            <tr>
                <td class="c">ราคาไม่รวมภาษีมูลค่าเพิ่ม</td>
                <td class="text-right">&nbsp;&nbsp;<?=number_format($Totaling-$vat, 2, '.', ',');?>&nbsp;บาท</td>
            </tr>
        </table>
    </div>
</div>


<div style="margin-top: 10px; font-size: 14px;"><b>หมายเหตุ</b></div>
<div style="margin-top: 5px; font-size: 12px;">อ้างอิงจาก ใบวางบิล เลขที่ </div>

<div class="row" style="margin-top:20px; font-size: 12px;">
    <div class="col-xs-5">
        <p>ในนาม &nbsp; </p>
    </div>
    <div class="col-xs-6">
        <p  class="text-right">ในนาม &nbsp;</p>
    </div>
</div>

<table class="table" style="padding-top: 10px;">
    <tr>
        <td style="padding-left:30px;">_______________</td>
        <td style="padding-left:5px;">__________________</td>
        <td style="padding-left:180px;">_______________</td>
        <td style="padding-left:5px;">__________________</td>
    </tr>
    <tr>
        <td style="padding-left: 60px; padding-top: 5px;">ผู้จ่ายเงิน</td>
        <td style="padding-left: 50px; padding-top: 5px;">วันที่</td>
        <td style="padding-left: 210px; padding-top: 5px;">ผู้รับเงิน</td>
        <td style="padding-left: 50px; padding-top: 5px;">วันที่</td>
    </tr>
</table>
<p  class="text-right">เราใช้  logo</p>
</body>
</html>
