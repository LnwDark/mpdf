
<?php


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<table class="table" width="100%">
    <thead>
    <tr>
        <th class="text-center">ลำดับ</th>
        <th class="text-center" >วันที่</th>
        <th class="text-center col-md-2">วันที่ใบกำกับ</th>
        <th class="text-right">สมุด</th>
        <th class="text-left col-md-1">เลขที่เอกสาร</th>
        <th class="text-left col-md-4 ">ชื่อผู้ซื้อสินค้า/ ผู้รับบริการสินค้า</th>
        <th class="text-center col-md-2">เลขประจำตัวผู้เสียภาษี</th>
        <th class="text-center col-md-2">สาขา</th>
        <th class="text-right">มูลค่าสินค้า / บริการ</th>
        <th class="text-right ">จำนวนเงินภาษี</th>
    </tr>
    </thead>
    <tbody>
    <?php
for ($i=0;$i<100;$i++): ?>
    <tr>
        <td class="text-center">1</td>
        <td class="text-center">12/02/2342</td>
        <td class="text-center">2016072101</td>
        <td >IV</td>
        <td class="text-center">2016072101222</td>
        <td class="text-center">จ่ายเงินสดให้กับบัริษัท เจเมกจำกัดddddddddddddd</td>
        <td class="text-center">0105559055912</td>
        <td>สานักงานใหญ</td>
        <td class="text-right">10,000.00</td>
        <td class="text-right">700.00</td>
    </tr>
    <?php endfor;?>
    </tbody>

</table>
<div class="line"></div>
<div class="col-xs-8 col-xs-offset-8 text-right">
    <table width="100%">
        <tr>
            <td width="33.33%">รวมหน้า</td>
            <td width="33.33%" class="text-right" >10000000000</td>
            <td width="33.33%" class="text-right">10000000000</td>
        </tr>
        <tr>
            <td width="33.33%">รวมทั้งสิ้น</td>
            <td width="33.33%" class="text-right" >10000000000</td>
            <td width="33.33%" class="text-right">10000000000</td>
        </tr>
    </table>
</div>
<div class="line"></div>


</body>
</html>
