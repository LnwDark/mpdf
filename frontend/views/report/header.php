<?php
$check1 = '<img src="mpdf/img/check1.png" width="15px" > ';
$check0 = '<img src="mpdf/img/box.png" width="15px" > ';

?>
<div class="text-center">
    <b style="font-size: 14px;">รายงานภาษีขาย</b>
    <p>เดือนภาษี  ปี </p>
</div>
<div class="text-left" >
    <div style="padding-left: 5px;font-weight:bold;">ชื่อผู้ประกอบการ</div>
    <div style="padding-left: 120px; margin-top: -16px;"><?=$company['name'] ?> </div>
    <div style="padding-left: 5px; font-weight:bold;">สถานที่ผู้ประกอบการ</div>
    <div style="padding-left: 120px; margin-top: -16px;"><?=$company['name'] ?></div>
    <div style="padding-left: 5px; margin-top: 10px; font-weight:bold;">ที่อยู่</div>
    <div class="spaceaddress"><?=($company['address'])?></div> <br/>
</div>

    <div class="text-right" style="margin-top:-75px;">
        <div style="padding-right: 150px; font-weight:bold; ">เลขที่ผู้เสียภาษี </div>
        <div style="padding-right:50px; margin-top: -15px"><?=$company['tax_id'] ?></div>
        <div style="padding-right: 150px;font-weight:bold;">เลขประจำตัวเสียภาษี (13 หลัก) </div>
        <div style="padding-right:50px; margin-top: -15px"><?=$company['tax_id'] ?></div>
        <br>
        <div style="padding-right: 150px">
<?= $company['branch'] == 1 ? $check1 : $check0;?>
        <span>สำนักงานใหญ่</span>
<?= $company['branch'] == 2 ? $check1 : $check0;?>
        <span>สาขาที่</span>
        </div>
        <div style="padding-right: 50px;margin-top: -15px;" class="text-right">หน้า {PAGENO} / {nbpg}</div>

