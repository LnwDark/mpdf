<?php
use yii\helpers\Html;

$name =
    array(
        'test1' => array('บัตรนักเรียน' => '2000', 'บัตรใหม่' => '10000'),
    );
?>

    <h3>ทดสอบภาษาไทย</h3>


<style>
    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }
    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }
    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }
    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }
    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }
</style>


<div class="col-sm-12">
    <div class="radio">
        <label>
            <input type="radio" name="o1" value="">
            <span class="cr">
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="o1" value="" checked>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            Option two is checked by default
        </label>
    </div>
    <div class="radio disabled">
        <label>
            <input type="radio" name="o1" value="" disabled>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            Option three is disabled
        </label>
    </div>
</div>

<div class="col-sm-12">
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option one is this and that — be sure to include why it's great
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="" checked>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option two is checked by default
        </label>
    </div>
    <div class="checkbox disabled">
        <label>
            <input type="checkbox" value="" disabled>
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Option three is disabled
        </label>
    </div>
</div>
