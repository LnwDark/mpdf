<?php if ($data['income_type'] == 0) : ?>
    <div class="row">

        <div class="col-xs-6">
            <div class="text-center">&nbsp;</div>
            <br/>
            <div class="lineL1"></div>
            <div class="textfootL1">ผู้สั่งซื้อสินค้า</div>
            <div class="linedate"></div>
            <div class="textdate">วันที่</div>
        </div>
        <div class="col-xs-5">
            <div class="text-right">ในนาม &nbsp;<?= $data['name']; ?> </div>
            <br/>
            <div class="linefootR1"></div>
            <div class="textfootR">ผู้อนุมัติ</div>
            <div class="linefootR2"></div>
            <div class="textdateR2">วันที่</div>
        </div>
    </div>
    <?php elseif($data['income_type'] == 1): ?>
    <div class="row">
        <div class="col-xs-6">
            <div class="text-center">&nbsp;</div>
            <br/>
            <div class="lineL1"></div>
            <div class="textfootL1">ผู้รับวางบิล</div>
            <div class="linedate"></div>
            <div class="textdate">วันที่</div>
        </div>
        <div class="col-xs-5">
            <div class="text-right">ในนาม &nbsp;<?= $data['name']; ?> </div>
            <br/>
            <div class="linefootR1"></div>
            <div class="textfootR22">ผู้วางบิล</div>
            <div class="linefootR2"></div>
            <div class="textdateR2">วันที่</div>
        </div>
    </div>
<?php elseif ($data['income_type'] == 2) : ?>
    <div>การชำระเงินจะสมบูรณ์เมื่อบริษัทได้รับเงินเรียบร้อยแล้ว</div>
    <div class="boxs1"><img src="mpdf/img/boxs.png" width="15px"></div>
    <div class="texrboxs2">เงินสด</div>
    <div class="boxs2"><img src="mpdf/img/boxs.png" width="15px"></div>
    <div class="texrboxs3">เช็ค</div>
    <div class="boxs3"><img src="mpdf/img/boxs.png" width="15px"></div>
    <div class="texrboxs4">โอนเงิน</div>
    <div class="boxs4"><img src="mpdf/img/boxs.png" width="15px"></div>
    <div class="texrboxs5">บัตรเครดิต</div>

    <div>ธนาคาร______________________เลขที่________________วันที่______________จำนวนเงิน____________________</div>
    <div class="row">
        <div class="col-xs-5">
            <div class="text-left">ในนาม &nbsp;<?= $data['Customer_name']; ?> </div>
            <br/>
            <div class="lineL1"></div>
            <div class="textfootL1">ผู้จ่ายเงิน</div>
            <div class="linedate"></div>
            <div class="textdateL1">วันที่</div>
        </div>
        <div class="col-xs-6 ">
            <div class="text-right">ในนาม &nbsp;<?= $data['name']; ?> </div>
            <br/>
            <div class="lineR1"></div>
            <div class="textR1">ผู้รับเงิน</div>
            <div class="lineR2"></div>
            <div class="textR2">วันที่</div>
        </div>
    </div>

<?php endif; ?>


        