<!doctype html>
<html lang="en">
<head>
    <?php
    $paperName = '';
    $Manuscript = '';
    if ($data['income_type'] == 0)  {
        $paperName = 'ใบเสนอราคา';
        $headCss='HSaneraka';

    } elseif ($data['income_type'] == 1) {
        $paperName = 'ใบวางบิล / ใบแจ้งหนี้';
        $headCss='headerinvo';
        $Manuscript = 'Tbilln';

    } elseif ($data['income_type'] == 2) {
        $paperName = 'ใบกำกับภาษี / ใบเสร็จรับเงิน';
        $headCss='headerinvo';
        $Manuscript = 'Tinvoder';

    }
    ?>
</head>
<body>

<?php $logo = 'ischool.png'; ?>
<img src="mpdf/img/Color.png" width="70px" class="lor">
<div class="pagenum"><?= $page == 1 ? '1' : '2'; ?></div>
<div class="logo">
    <img src="mpdf/img/logo/<?= $logo ?>">
</div>
<div class="<?=$headCss?>"><?= $paperName ?></div>

<?php if($data['income_type']==1 || $data['income_type']==2):?>
<div class="<?= $Manuscript ?>"><?= $page == 1 ?'ต้นฉบับ':'สำเนา'; ?></div>
<?php endif;?>
<div class="row">
    <div class="col-xs-6">
        <div class="row">
            <div style="font-size: 15px"><?= $data['name']; ?></div>
            <div><?= 'เลขที่' . ' ' . $data['address']; ?></div>
            <div><?= $data['sub_district'] . '  ' . $data['district'] . '  ' . $data['province'] . '  ' . $data['postal_code']; ?></div>
            <div><?= 'โทร' . ' ' . $data['telephone']; ?></div>
            <div><?= 'เลขประจำตัวผู้เสียภาษี' . '  ' . $data['tax_id'] . '  (' . $data['branch'] . ')' ?></div>
            <div class="customers">
                <div class="custromertest">ลูกค้า</div>
                <div><?= $data['Customer_name'] ?></div>
                <div><?= $data['Customer_address'] ?></div>
                <div><?= 'เลขประจำตัวผู้เสียภาษี' . '  ' . $data['Customer_tax_id'] ?></div>
            </div>
        </div>
    </div>
    <div id="up-triangle" class="up-triangle"></div>
    <div class="col-xs-5">
        <div class="linetop"></div>
        <div>เลขที่</div>
        <div class="texti"><?= $data['Code_Bill'] ?></div>
        <div>วันที่</div>
        <div class="texti"><?= $data['Date_Bill'] ?></div>
        <div>ครบกำหนด</div>
        <div class="texti"><?= $data['Date_duly'] ?></div>
        <div class="linetop"></div>
    </div>
</div>

<div class="col-xs-12">
    <table class="tables">
        <thead>
        <tr>
            <th width="10%" class="text-center">#</th>
            <th width="40%" class="text-center">รายละเอียด</th>
            <th width="10%" class="text-right">จำนวน</th>
            <th width="20%" class="text-right">ราคาต่อหน่วย</th>
            <th width="25%" class="text-right">ยอดรวม</th>
        </tr>
        </thead>
        <tbody>
        <?php $datatest = [
            ['id' => '1', 'name' => 'ค่าบริการเครื่องและบัตรระบบช่วยเหลือนักเรียน  iSchool เทมอ 2 ปี ปีการศึกษา 2559 คนล่ะ 9000000', 'price' => '90', 'amount' => '9'],
            ['id' => '2', 'name' => 'ชำรุด', 'price' => '90', 'amount' => '10000000'],
            ['id' => '100000000', 'name' => 'ชำรุด', 'price' => '10000000', 'amount' => '10']
        ]; ?>
        <?php
        $Totaling = 0;
        ?>
        <?php foreach ($datatest as $model): ?>
            <tr>
                <td class="text-center"><?= $model['id'] ?></td>
                <td><?= $model['name'] ?></td>
                <td class="text-right"><?= $model['amount'] ?></td>
                <td class="text-right"><?= number_format($model['price'], 2) ?></td>
                <td class="text-right"><?= number_format($sum = $model['amount'] *= $model['price'], 2) ?></td>
                <?php $Totaling += $sum ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="texttotal"> รวมทั้งสิ้น</div>
<div class="textprice"><?= number_format($Totaling, 2) ?> บาท</div>
<div class="textvat"> ภาษีมูลค่าเพิ่ม 7 %</div>
<div class="textprice"><?= number_format($Totaling * 0.07, 2) ?> บาท</div>
<div class="textnotvat"> ราคาไม่รวมภาษีมูลค่าเพิ่ม</div>
<div class="textprice"><?= number_format($Totaling * 0.07, 2) ?> บาท</div>
<div class="textall"> รวมจำนวนเป็นเงิน</div>
<div class="textprice"><?= number_format($Totaling, 2) ?> บาท</div>
<div class="linefooter"></div>
<div class="asone"><?= FormatBaht($Totaling) ?></div>
<?php if ($data['income_type'] == 1 || $data['income_type'] ==0) : ?>
<div class="col-xs-11">
    <div class="maihed">หมายเหตุ</div>
    <div class="textmaihed"><?= $data['Note'] ?></div>
    <?php endif; ?>
</div>
</body>
</html>
