<?php
$today = date("Y-m-d H:i:s");
$company=[
    'type'=>3,  // 1=>จ่าย 2=>ทั่วไป 3=>รับ
    'name'=>'บริษัท เนกเจน แอนด์ เน็ทออพ จำกัด',
    'refer'=>'N18200',
    'number'=>'12312',
    'date'=> '12/08/2534',
    'details'=>'การจ่าย อุปกรณ์ -เครื่องใช้สำนักงานการจ่าย อุปกรณ์ -เครื่องใช้สำนักงานุปกรณ์ -เครื่องใช้สำนักงาน รายละเอียดการจ่าย อุปกรณ์ -เครื่องใช้สำนักงาน'
];
$PagenameTH='';
$PagenameEN='';
$ReferNumber='';
if($company['type']==1){
    $PagenameTH='ใบสำคัญจ่าย';
    $PagenameEN='PAYMENT VOUCHER';
}
elseif($company['type']==2){
    $PagenameTH='ใบสำคัญทั่วไป';
    $PagenameEN='Gereral VOUCHER';
}
elseif($company['type']==3){
    $PagenameTH='ใบสำคัญรับ';
    $PagenameEN='RECEIPT VOUCHER';}
    $ReferNumber='';
?>
<!doctype html>
<html lang="en">
<head>
</head>
<body>
<div class="text-center"><h4><?=!empty($company['name'])?$company['name']:'&nbsp;'?></h4></div>
<div class="row">
    <div class="col-xs-6">
        <div style="padding-left: 20px;">
            <h3><?=$PagenameTH?></h3>
            <div class="textEn"><?=$PagenameEN?></div>
        </div>
    </div>
    <div class="col-xs-4 numberR" >
        <div class="num1">เลขที่ </div>
        <div  class=" top1"><?=!empty($company['number'])?$company['number']:'&nbsp;'?></div>
        <div class="num1">วันที่ </div>
        <div  class=" top1"><?=!empty($company['date'])?$company['date']:'&nbsp;'?></div>
    </div>
</div>
<div class="detail-list">

    <span><b>รายละเอียด</b> </span><?= isset($company['details'])?$company['details']:''?>
    <?php if($company['type']==3):?>
        <br>
        <?= '<b>อ้างอิงใบเสร็จรับเงินเลขที่</b>'.' '.$company['refer']?>
    <?php endif;?>
</div>
</body>
</html>