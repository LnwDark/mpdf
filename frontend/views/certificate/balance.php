<?php
$balance = [
    'Name_Company' => 'เน็กซ์เจน',
    'date' => date('d/m/Y'),
];
$asset = [
    ['name' => 'เงินสด', 'price' => '20030'],
    ['name' => 'ลูกหนี้', 'price' => '30000'],
];
$Not_asset = [
    ['name' =>'อุปกรณ์ช่าง', 'price' => '60000'],
    ['name' =>'ขนมพนักงาน', 'price' => '220000'],
    ['name' =>'ค่าอาหาร', 'price' => '50000'],
];
$Lank_Spin=[
    ['name' =>'เจ้าหนี้', 'price' => '6000'],
];
$Lank_NotSpin=[
    ['name' =>'เงินกู้', 'price' => '520000'],
];
$sumAsset = 0;
$sumNotAsset = 0;
$Total_Spin=0;
$Total_NotSpin=0;
$Research=200;
$Positive=100;
$Earning=1200;
$Total_owner=0;
?>

<!doctype html>
<html lang="en">
<head>
</head>
<body>
<div class="text-center Btxth1">ร้าน
    <span><?= !empty($balance['Name_Company']) ? $balance['Name_Company'] : '..................................' ?></span>
</div>
<div class="text-center Btxth2">งบดุล</div>
<div class="text-center Btxth3">ณ วันที่
    <span><?= !empty($balance['date']) ? $balance['date'] : '..................................' ?></span></div>
<div class="text-center Btxth4">สินทรัพย์</div>
<div>สินทรัพย์หมุนเวียน</div>
<div class="row">
    <div class="col-xs-5">
        <table class="incomes">
            <?php foreach ($asset as $a): ?>
                <tr>
                    <td style="text-align: left"><?= $a['name'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <?php foreach ($asset as $a): ?>
                <tr>
                    <td><?= number_format($a['price']) ?></td>
                    <?php $sumAsset += $a['price']; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <div class="Ltxt">รวมสินทรัพย์หมุนเวียน</div>
    </div>
    <div class="col-xs-6">
        <div class="newsz ">
            <div class="TopdownRB"><?= !empty($sumAsset) ? number_format($sumAsset, 2) : '' ?></div>
        </div>
    </div>
</div>
<div style="margin-top: 5px">สินทรัพย์ไม่หมุนเวียน</div>
<div class="row">
    <div class="col-xs-5">
        <table class="incomes">
            <?php foreach ($Not_asset as $not): ?>
                <tr>
                    <td style="text-align: left"><?=@$not['name'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <?php foreach ($Not_asset as $not): ?>
                <tr>
                    <td><?= number_format($not['price']) ?></td>
                    <?php $sumNotAsset += $not['price']; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <div class="Ltxt">รวมสินทรัพย์ไม่หมุนเวียน</div>
    </div>
    <div class="col-xs-6">
        <div class="newsz ">
            <div class="TopdownRB"><?= !empty($sumNotAsset) ? number_format($sumNotAsset, 2) : '' ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <div class="down-slightly">รวมสินทรัพย์</div>
    </div>
    <div style="margin-left: 70%; margin-top: -20px;">
        <div class="newsz">
            <div class="TopdownRB"><?= !empty($sumAsset+$sumNotAsset) ? number_format($sumAsset+$sumNotAsset, 2) : '' ?></div>
        </div>
    </div>
</div>

<div class="text-center Btxth4">หนี้สินและส่วนของเจ้าของ</div>
<div class="down-slightly">หนี้สินหมุนเวียน</div>
<div class="row">
    <div class="col-xs-5">
        <table class="incomes">
            <?php foreach ($Lank_Spin as $sp): ?>
                    <tr>
                        <td style="text-align: left"><?= $sp['name'] ?></td>
                    </tr>
                <?php endforeach; ?>
        </table>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <?php foreach ($Lank_Spin as $sp): ?>
                <tr>
                    <td><?= number_format($sp['price']) ?></td>
                    <?php $Total_Spin += $sp['price']; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div class="down-slightly">หนี้สินไม่หมุนเวียน</div>
<div class="row">
    <div class="col-xs-5">
        <table class="incomes">
            <?php foreach ($Lank_NotSpin as $nsp): ?>
                <tr>
                    <td style="text-align: left"><?= $nsp['name'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <?php foreach ($Lank_NotSpin as $nsp): ?>
                <tr>
                    <td><?= number_format($nsp['price']) ?></td>
                    <?php $Total_NotSpin += $nsp['price']; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <div class="down-slightly">รวมหนี้สิน</div>
    </div>
    <div style="margin-left: 70%; margin-top: -20px;">
        <div class="newsz">
            <div class="TopdownRB"><?= !empty($Total_Spin+$Total_NotSpin) ? number_format($Total_Spin+$Total_NotSpin, 2) : '' ?></div>
        </div>
    </div>
</div>

<div class="down-slightly">ส่วนของเจ้าของ</div>
<div class="row">
    <div class="col-xs-5">
        <div class="text-downL">ทุน-นายวินัย</div>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <tr>
                <td><?= !empty($Research) ? number_format($Research) : '' ?></td>
            </tr>
        </table>
    </div>
    <div class="col-xs-5">
        <div class="text-downL"><span style="text-decoration:underline">บวก</span> กำไรสุทธิ</div>
    </div>
    <div class="col-xs-6">
        <table class="blan">
            <tr>
                <td><?= !empty($Positive) ? number_format($Positive) : '' ?></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black"><?= !empty($Positive) ? number_format($Positive) : '' ?></td>
            </tr>
        </table>
    </div>
    <div class="col-xs-5">
        <div class="text-downL" style="margin-top: 40px"><span style="text-decoration:underline">หัก</span>
            ถอนใช้ส่วนตัว
        </div>
    </div>
    <div class="col-xs-6">
        <table class="blan" style="margin-top: 40px">
            <tr>
                <td><?= !empty($Earning) ? number_format($Earning) : '' ?></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black"><?= !empty($Earning) ? number_format($Earning) : '' ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-5">
        <div class="down-slightly">รวมหนี้สินและส่วนของเจ้าของ</div>
    </div>
    <div style="margin-left: 70%; margin-top: -20px;">
        <div class="newsz">
            <div class="TopdownRB"><?= !empty($Total_owner) ? number_format($Total_owner, 2) : '' ?></div>
        </div>
    </div>
</div>


</body>
</html>