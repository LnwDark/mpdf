<?php
$data=[
  'school_name'=>'พานหัวพาด',
    'province'=>'จ.หลังป่า'
];
?>
<!doctype html>
<html lang="en">
<head>
</head>
<body>
<div class="text-center">
<img src="https://goo.gl/GfHQMa" width="120">
</div>
<div class="text-center" style="font-size: 16px; font-weight: bold;"> ใบตรวจรับงานส่งมอบบัตรประจำตัวนักเรียน</div>
<div class="text-right" style="margin-top: 10px;"> วันที่....เดือน......พ ศ 2559</div>
<div class="texthr">ด้วยบริษัท  เนกเจนแอนด์เน็ทออพ จำกัด  ได้ดำเนินการส่งมอบบัตรประจำตัวนักเรียน เพื่อใช้กับระบบดูแลช่วยเหลือ</div>
<div> นักเรียน(iSchool) ให้กับ  <?=!empty($data['school_name'])?'<b>โรงเรียน'.$data['school_name'].'</b>':'.......................'?> ที่อยู่ <?=!empty($data['province'])?'<b>'.$data['province'].'</b>':'.......................'?></div>
<div class="">ชื่อผู้ติดต่อ .......................</div>
<div class="row" style="margin-top: 10px">
    <div class="col-xs-5">
        <table class="table">
            <thead>
            <tr>
                <th>รายการ</th>
                <th>จำนวน / ใบ</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>- บัตรใหม่</td>
                <td class="text-center">100</td>
            </tr>
            <tr>
                <td>- บัตรหาย</td>
                <td class="text-center">-</td>
            </tr>
            <tr>
                <td>- ข้อมูลไม่ถูกต้อง</td>
                <td class="text-center">-</td>
            </tr>
            <tr>
                <td>- สแกนบัตรไม่ติด</td>
                <td class="text-center">-</td>
            </tr>
            <tr>
                <td>- บัตรสำรอง</td>
                <td class="text-center">-</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-6">
        <table class="table">
            <thead>
            <tr>
                <th>บัตรประจำตัวนักเรียน</th>
                <th>จำนวน / ใบ</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>- ระดับชั้น ม.1</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td>- ระดับชั้น ม.2</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td>- ระดับชั้น ม.3</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td>- ระดับชั้น ม.4</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td>- ระดับชั้น ม.5</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td>- ระดับชั้น ม.6</td>
                <td class="text-center">12</td>
            </tr>
            <tr>
                <td class="Bolds">รวมเป็นจำนวนทั้งสินค้า</td>
                <td class="text-center">2000</td>
            </tr>

            </tbody>
        </table>
    </div>
</div>
<div class="texthr" >ทางบริษัท เนกเจนแอนด์เน็ทออพ จำกัด ได้ดำเนินการส่งมอบบัตรประจำตัวนักเรียนเป็นที่เรียบร้อยและ รับรอง</div>
<div>ว่าถูกต้องทุกประการ จึงลงลายมือชื่อไว้เป็นสำคัญ</div>

<div class="row" style="margin-top: 10px;">
    <div class="col-xs-6">
        <div>ลงชื่อ..............................................ผู้รับส่งมอบงาน</div>
        <div style="margin-left: 30px; margin-top: 5px">(..............................................)</div>
        <div style="margin-left: 5px;margin-top: 5px;">วันที่.........../............/.....................</div>
    </div>
    <div style="margin-left: 20%">
        <div>ลงชื่อ..............................................ผู้รับส่งมอบงาน</div>
        <div style="margin-left: 30px; margin-top: 5px">(..............................................)</div>
        <div style="margin-left: 5px;margin-top: 5px;">วันที่.........../............/.....................</div>
    </div>
</div>

<div style="margin-top: 10px">
    <div class="Bolds">รายชื่อนักเรียนที่สั่งพิมพ์บัตร</div>
    <table class="detail" >
        <thead>
        <tr>
            <th width="10%">ลำดับ</th>
            <th width="15%">รหัสนักเรียน</th>
            <th width="40%">ชื่อ - นามสกุล</th>
            <th width="20%">คำขอ</th>
            <th width="10%">ระดับชั้น</th>
            <th width="10%">ห้องเรียน</th>
            <th width="20%">สั่งเมื่อ</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i=0; $i <14; $i++): ?>
        <tr>
            <td class="text-center"><?=$i+1?></td>
            <td class="text-center">552001</td>
            <td>นายการเรียนดี นาทีทองปองสินกินนะรินใจ</td>
            <td class="text-center">บัตรหายอะไร</td>
            <td class="text-center" >ม.1</td>
            <td class="text-center">1</td>
            <td> 10 /2/ 2500</td>

        </tr>
        <?php endfor;?>
        </tbody>
    </table>
</div>

</body>
</html>