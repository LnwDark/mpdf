<?php
$data = [
    'name' => 'Nextgensoft',
    'date' => '',
];
$income = [
    ['name' => 'ค่าบริการต่างๆ', 'price' => 2000],
    ['name' => 'เก็บค่าบริการบัตรโรงเรียน', 'price' => 1999],
    ['name' => 'ค่าบริการต่างๆ', 'price' => 123],
];
$spend = [
    ['id' => '1', 'name' => 'ค่าน้ำ-ค่าไฟ', 'price' => 20000],
    ['id' => '1', 'name' => 'ค่าโทรศัพท์', 'price' => 50499],
    ['id' => '1', 'name' => 'เงินเดือน', 'price' => 90000],
];
$SumIncome = 0;
$SumSpend = 0;
$TotalCost=0; //รวมค่าใช้จ่าย
$Net_profit=0;  //กำไรสุทธิ

?>
<!doctype html>
<html lang="en">
<head>
</head>
<body>
<div class="Stxth1">ร้าน<span> <?= !empty($data['name']) ? $data['name'] : '..................' ?></span></div>
<div class="Stxth2">งบกำไรขาดทุน</div>
<div class="Stxth3">สำหรับระยะเวลา 1 ปี สิ้นสุดวันที่<span><?= !empty($data['date']) ? $data['date'] : '..................' ?></span></div>
<div class="income">รายได้</div>

<div class="row">
    <div class="col-xs-5">
        <?php foreach ($income as $model): ?>
            <table class="incomes">
                <tr>
                    <td style=""><?= $model['name'] ?></td>
                    <?php $SumIncome += $model['price'] ?>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-5">
        <div class="space">รวมรายได้</div>
    </div>
    <div class="col-xs-4">
        <div class="newsum ">
            <div class="TopdownR" tyle=""><?= !empty($SumIncome) ? number_format($SumIncome, 2) : '' ?></div>

        </div>
    </div>
</div>

<div class="income">ค่าใช้จ่าย</div>
<div class="row">
    <div class="col-xs-5">
        <?php foreach ($spend as $models): ?>
            <table class="incomes">
                <tr>
                    <td><?= $models['name'] ?></td>
                    <?php $SumSpend += $models['price'] ?>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <div class="space">รวมค่าใช้จ่าย</div>
    </div>
    <div class="col-xs-4">
        <div class="newsum ">
            <div class="TopdownR" tyle=""><?= !empty($SumSpend) ? number_format($SumSpend, 2) : '' ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
        <div class="space"> กำไรสุทธิ (ขาดทุนสุทธิ)</div>
    </div>
    <div class="col-xs-7 text-center">
        <div style="margin-left:250px; ">
            <div class="totalincome"><?= !empty($TotalCost) ? number_format($TotalCost, 2) : '&nbsp;' ?></div>

        </div>
        <div style="margin-left:250px; ">
            <div class="totalincome"><?= !empty($Net_profit) ? number_format($Net_profit, 2) : '&nbsp;' ?></div>

        </div>
    </div>
</div>

</body>
</html>