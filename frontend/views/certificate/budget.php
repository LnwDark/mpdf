<?php
$data = [
    ['name' => 'โรงเรียนปากน้ำครอง', 'CR' => 12313.5, 'DR' => 2000],
    ['name' => 'โรงเรียนปากน้ำ', 'CR' => 32313.42, 'DR' => 5000]
];
$Totaldebit=0;
$Totalcredit=0;
?>

<!doctype html>
<html lang="en">
<head>
</head>
<body>
<table class="budget" width="100%">
    <thead>
    <tr>
        <th rowspan="2" width="50%">ชื่อบัญชี</th>
        <th rowspan="2" width="10%">เลขที่บัญชี</th>
        <th colspan="2">เดบิต</th>
        <th colspan="2">เครบิต</th>
    </tr>
    <tr>
        <th>บาท</th>
        <th width="50px">สต</th>
        <th>บาท</th>
        <th width="50px">สต</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $model): ?>
        <?php
        $stDebit = $model['CR']  * 100 %100;
        $stCredit = $model['DR']  * 100 %100;
        $Totaldebit+=$model['CR'];
        $Totalcredit+=$model['DR'];
        ?>

        <tr>
            <td class="text-left">โรงเรียนการทำงานระบบที่เราบอกเชาไปได้จากระบบ</td>
            <td class="text-center"><?= +1 ?></td>
            <td class="text-right"><?= number_format($model['CR']) ?></td>
            <td class="text-center"><?= !empty($stDebit)?$stDebit:'-' ?></td>
            <td class="text-right"><?= number_format($model['DR']) ?></td>
            <td class="text-center"><?= !empty($stCredit)?$stCredit:'-' ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <td></td>
        <td></td>
        <td class="text-right"><?= !empty($Totaldebit)?number_format($Totaldebit):'-' ?></td>
        <td></td>
        <td class="text-right"><?= !empty($Totalcredit)?number_format($Totalcredit):'-' ?></td>
        <td></td>
    </tr>
    </tfoot>

</table>
</body>
</html>
