<?php
$data = [
	['id'=>'1','name'=>'บริษัท ไปรษณีย์ไทย จํากัด','DR'=>'10022','CR'=>'29'],
	['id'=>'2','name'=>'บริษัท เมกะ แอ๊ดวานซ์ เทค จำกัด','DR'=>'1002','CR'=>'1022'],
	['id'=>'3','name'=>'บริษัท กรุงเทพประกันภัย จำกัด (มหาชน)','DR'=>'1002','CR'=>'120'],
	['id'=>'2','name'=>'บริษัท เมกะ แอ๊ดวานซ์ เทค จำกัด','DR'=>'1002','CR'=>'1022'],
	['id'=>'3','name'=>'บริษัท กรุงเทพประกันภัย จำกัด (มหาชน)','DR'=>'1002','CR'=>'120'],

];
$num=1;
$minus=0;
$minus=count($data);
?>
<!doctype html>
<html lang="en">
<head>
</head>
<body>
<div style="margin-top: 10px">
<table  class="table1"   width="100%">
	<thead>
		<tr>
			<th width="15%" class="text-center">ลำดับที่</th>
			<th width="20%" class="text-center">รหัส</th>
			<th width="70%" class="text-center">รายการ / Description</th>
			<th width="20%" class="text-center">DR</th>
			<th width="20%" class="text-center">CR</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($data as $value): ?>
		<tr>
			<td class="text-center" ><?=$num++?></td>
			<td class="text-center"><?=$value['id']?></td>
			<td><?=$value['name']?></td>
			<td class="text-right"  ><?=number_format($value['DR'],2)?></td>
			<td class="text-right"><?=number_format($value['CR'] ,2)?></td>
		</tr>
	<?php endforeach;?>
		<?php for($x = 0; $x <= (20-$minus); $x++):?>
		<tr>
			<td class="text-center" ></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="text-right">&nbsp;</td>
			<td class="text-right">&nbsp;</td>
		</tr>
	<?php endfor;?>
	</tbody>
	<tfoot>
	<tr>
		<th colspan="2" class="tdrum"></th>
		<th class="td33"  >รวม</th>
		<th class="td33"></th>
		<th class="td33"></th>
	</tr>
	</tfoot>
</table>
</div>
</body>
</html>