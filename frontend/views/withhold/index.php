<?php
use yii\helpers\Html;

$this->registerCssFile("@web/css/withhold.css");
$company = [
    'name' => 'บริษัท เนกเจน แอนด์ เนทออพ จำกัด',
    'address' => '1558/41 ถ.บางนา-ตราด',
    'sub_district' => 'แขวงบางนา',
    'district' => 'เขตบางนา',
    'province' => 'กรุงเทพฯ',
    'postal_code' => '10260',
    'tax_id' => '0105559127204111',
    'country' => 'Thailand',
    'branch' => 'สำนักงานใหญ่',
    'telephone' => '02-101-4822',
    'number' => 'bi20160123',
    'type' => 4,
    'PayerType' => 1,
];
$supplier = [
    'name' => 'นายสุริชัย โพธิ์ประโคน',
    'tax_id' => '1310800155260',
    'address' => '24 หมู่2 ต.บึงเจริญ อ.บ้านกรวด จ.บุรีรัมย์',
    'note' => 'รายละเอียดข้อมูล',
];
$money = [
    'wht' => 450,
    'baht' => 15000,
    'Check_tax' => '28/11/2559',
    'Date_issue' => '30/11/2559',
];

?>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
<div class="previewContainer">
    <div class='invoice-page-wrap'>
        <div style="border: 1px solid black; padding: 0 5px">
            <div style="text-align:center;">
                <span class="header-wt" style="margin: 0; font-size: 16px;font-weight:bold;color: #141615;">หนังสือรับรองการหักภาษี ณ ที่จ่าย </span>
                <span style="font-size: 14px;position: absolute;padding-top: 2px;right: 52px; margin-top: 10px">เลขที่
                 <span class="dotted incomeOther" style="word-wrap: break-word;width:80px;">
                                    <?= $company['number'] ?>
                                </span>
                </span>
            </div>
            <div class="pdf-margin" style="text-align: center; font-size: 14px; padding-bottom:3px; ">
                ตามมาตรา 50 ทวิ แห่งประมวณรัษฎากร
            </div>
            <div>
                <div class="section2box">
                    <div style="margin: 7px; margin-top: 5px;">
                        <label class="indentspan">ผู้มีหน้าที่หักภาษี ณ ที่จ่าย : -</label>
                        <?php if (strlen($company['tax_id']) == 10): ?>
                            <div
                                style="margin-top:-20px; text-align: right;padding-right: 200px; font-size: 11px; font-weight: bold;  ">
                                เลขประจำตัวผู้เสียภาษีอากร
                            </div>
                            <div style="float: right;  font-size: 12px; margin-top: -15px">
                                <div class="numblock1" style="margin-top: -2px;"><?= $company['tax_id'][0]; ?></div>
                                <span>-</span>
                                <div class="numblock1"><?= $company['tax_id'][1]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][2]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][3]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][4]; ?></div>
                                <span class="to" style="margin-left:0;">-</span>
                                <div class="numblock"><?= $company['tax_id'][5]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][6]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][7]; ?></div>
                                <div class="numblock"><?= $company['tax_id'][8]; ?></div>
                                <span class="to">-</span>
                                <div class="numblock"><?= $company['tax_id'][9]; ?></div>
                            </div>
                        <?php else: ?>

                        <?php endif; ?>

                        <div style="height: 87px">
                            <div style="padding-top: 10px;">
                                <span class="indentspan">ชื่อ</span>
                                <span class="dotted incomeOther" style="word-wrap: break-word;width:350px;">
                                    <?= !empty($company['name']) ? $company['name'] : '' ?>
                                </span>
                            </div>
                            <?php if (strlen($company['tax_id']) == 13): ?>
                                <div class="number13">
                                    เลขประจำตัวผู้เสียภาษีอากร
                                </div>
                                <div class="boxnumber13">
                                    <div class="numblock1" style="margin-top: -2px;"><?= $company['tax_id'][0]; ?></div>
                                    <span>-</span>
                                    <div class="numblock1"><?= $company['tax_id'][1]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][2]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][3]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][4]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $company['tax_id'][5]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][6]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][7]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][8]; ?></div>
                                    <span class="to">-</span>
                                    <div class="numblock"><?= $company['tax_id'][9]; ?></div>
                                </div>
                            <?php else: ?>
                                <div class="number13">
                                    เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*
                                </div>
                                <div class="boxnumber13">
                                    <div class="numblock1" style="margin-top: -2px;"><?= $company['tax_id'][0]; ?></div>
                                    <span>-</span>
                                    <div class="numblock1"><?= $company['tax_id'][1]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][2]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][3]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][4]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $company['tax_id'][5]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][6]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][7]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][8]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][9]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $company['tax_id'][10]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][11]; ?></div>
                                    <span class="to">-</span>
                                    <div class="numblock"><?= $company['tax_id'][12]; ?></div>
                                </div>
                            <?php endif; ?>

                            <div style="margin-top: 40px;">
                                <span class="indentspan">ที่อยู่</span>
                                <span class="dotted incomeOther" style="word-wrap: break-word;width:640px;">
                                  <?= !empty($company['address']) ? $company['address'] . '
                                    ' . ' ต.' . $company['sub_district'] . '  
                                    ' . ' อ.' . $company['district'] . ' 
                                    ' . ' จ.' . $company['province'] . ' 
                                    ' . '  ' . $company['postal_code'] . '
                                   ' : '' ?>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="section3box" style="margin-left: 0px; ">
                    <div style="margin: 7px; margin-top: 5px;">
                        <label class="indentspan">ผู้ถูกหักภาษี ณ ที่จ่าย :-</label>

                        <div style="height: 87px">
                            <div style="padding-top: 10px;">
                                <span class="indentspan">ชื่อ</span>
                                <span class="dotted incomeOther" style="word-wrap: break-word;width:520px;">
                                   <?= !empty($supplier['name']) ? $supplier['name'] : '' ?>
                                </span>
                            </div>
                            <?php if (strlen($supplier['tax_id']) == 10): ?>
                                <div class="number13">
                                    เลขประจำตัวผู้เสียภาษีอากร
                                </div>
                                <div class="boxnumber13">
                                    <div class="numblock1"
                                         style="margin-top: -2px;"><?= $supplier['tax_id'][0]; ?></div>
                                    <span>-</span>
                                    <div class="numblock1"><?= $supplier['tax_id'][1]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][2]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][3]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][4]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $supplier['tax_id'][5]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][6]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][7]; ?></div>
                                    <div class="numblock"><?= $supplier['tax_id'][8]; ?></div>
                                    <span class="to">-</span>
                                    <div class="numblock"><?= $supplier['tax_id'][9]; ?></div>
                                </div>
                            <?php else: ?>
                                <div class="number13">
                                    เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)*
                                </div>
                                <div class="boxnumber13">
                                    <div class="numblock1"
                                         style="margin-top: -2px;"><?= $supplier['tax_id'][0]; ?></div>
                                    <span>-</span>
                                    <div class="numblock1"><?= $company['tax_id'][1]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][2]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][3]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][4]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $company['tax_id'][5]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][6]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][7]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][8]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][9]; ?></div>
                                    <span class="to" style="margin-left:0;">-</span>
                                    <div class="numblock"><?= $company['tax_id'][10]; ?></div>
                                    <div class="numblock"><?= $company['tax_id'][11]; ?></div>
                                    <span class="to">-</span>
                                    <div class="numblock"><?= $company['tax_id'][12]; ?></div>
                                </div>
                            <?php endif; ?>
                            <div style="padding-top: 40px;">
                                <span class="indentspan">ที่อยู่</span>
                                <span class="dotted incomeOther" style="word-wrap: break-word;width:640px;">
                                    <?= !empty($supplier['address']) ? $supplier['address'] : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="article">
                <div class="address" style="margin-top: 2px; width: 100%;padding-left: 13px;">
                    <table style="width: 100%;">
                        <tr>
                            <td width="13%">ลำดับที่.........ในแบบ</td>

                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment" <?= $company['type'] == 1 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span style="vertical-align: bottom;line-height: 17px;">(1) ภ.ง.ด.1ก</span></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment" <?= $company['type'] == 2 ? 'checked=&#39;checked&#39;' : '' ?>>
                                <span style="vertical-align: bottom;line-height: 17px;">(2) ภ.ง.ด.1ก พิเศษ </span></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment"<?= $company['type'] == 3 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span style="vertical-align: bottom;line-height: 17px;">(3) ภ.ง.ด.2</span></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment" <?= $company['type'] == 4 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span>(4) ภ.ง.ด.3</span></td>
                        </tr>
                        <tr>
                            <td width="13%"></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment"<?= $company['type'] == 5 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span style="vertical-align: bottom;line-height: 17px;">(5) ภ.ง.ด 2ก</span></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment"<?= $company['type'] == 6 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span style="vertical-align: bottom;line-height: 17px;">(6) ภ.ง.ด3ก</span></td>
                            <td width="13%"><input type="checkbox"
                                                   class="large whtAttachment" <?= $company['type'] == 7 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span style="vertical-align: bottom;line-height: 17px;">(7) ภ.ง.ด.53</span></td>

                        </tr>
                    </table>
                </div>
                <div class="linesatang"></div>
                <div class="linesatang" style="right: 70px;"></div>
                <table class="bordered inventory pdf-margin header-table-wt">
                    <thead>
                    <tr style="vertical-align: middle; border: 1px solid black;">
                        <th style="width: 400px; font-size: 13px;border-color: black; ">
                            ประเภทเงินได้พึงประเมินที่จ่าย
                        </th>
                        <th style="font-size: 13px;border-color: black; width:  ">วัน เดือน หรือ ปีภาษีที่จ่าย</th>
                        <th style="font-size: 13px;border-color: black; ">จำนวนเงินที่จ่าย</th>
                        <th style="font-size: 13px;border-color: black; ">ภาษีที่หักและ <br/>นำส่งไว้</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="no-bottom" style="padding-right: 10px;">
                            1. เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ ตามมาตรา 40(1)
                        </td>

                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom" style="padding-right: 10px;">
                            2. ค่าธรรมเนียม ค่านายหน้า ฯลฯ ตามมาตรา 40(2)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom" style="padding-right: 10px;">
                            3. ค่าแห่งลิขสิทธิ์ ฯลฯ ตามมาตรา 40(3)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom" style="padding-right: 10px;">
                            4. (ก) ค่าดอกเบี้ย ฯลฯ ตามมาตรา 40(4)(ก)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl1" style="padding-right: 10px;">
                            (ข) เงินปันผล เงินส่วนแบ่งกำไร ฯลฯ ตามมาตรา 40(4)(ข) ที่จ่ายจาก
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl2" style="padding-right: 10px;">
                            (1) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราดังนี้
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            (1.1) อัตราร้อยละ 30 ของกำไรสุทธิ
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            (1.2) อัตราร้อยละ 25 ของกำไรสุทธิ
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            (1.3) อัตราร้อยละ 20 ของกำไรสุทธิ
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            (1.4) อัตราอื่นๆ (ระบุ)
                            <span class="dotted rateOther">
                        </span>
                            ของกำไรสุทธิ
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl2" style="padding-right: 10px;">
                                <span
                                    class="indentspan">(2) กรณีผู้ได้รับเงินปันผลไม่ได้รับเครดิตภาษี เนื่องจากจ่ายจาก</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            <span class="indentspan">(2.1) กำไรสุทธิของกิจการที่ได้รับยกเว้นเงินได้นิติบุคคล</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            <span class="indentspan">(2.2)</span>
                            <span class="indentinline" style="width: 300px;word-wrap:break-word;"> เงินปันผลหรือเงินส่วนแบ่งของกำไรที่ได้รับยกเว้นไม่ต้องนำมารวมคำนวนเป็นรายได้เพื่อเสียภาษีเงินได้นิติบุคคล</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                            <span class="indentspan">(2.3)</span>
                            <span class="indentinline" style="width: 300px;"> กำไรสุทธิส่วนที่ได้หักผลขาดทุนสิทธิยกมาไม่เกิน 5 ปีก่อนรอบระยะเวลาบัญชีปีปัจจุบัน</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                                <span
                                    class="indentspan">(2.4) กำไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="no-bottom indentlvl3" style="padding-right: 10px;">
                        <span class="indentspan">(2.5) อื่นๆ (ระบุ)
                            <span class="dotted" style="min-width: 250px;">

                        </span>
                        </span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="no-bottom" style="padding-right: 10px;">
                            <span class="indentspan">5.&nbsp;</span>
                            <span class="indentinline2" style="width: 360px;">
                            การจ่ายเงินได้ที่ต้องหักภาษี ณ ที่จ่ายตามคำสั่งกรมสรรพากรที่ออกตามมาตรา 3 เตรส เช่น รางวัล ส่วนลดหรือประโยชน์ใดๆ
                            เนื่องจากการส่งเสริมการขาย รางวัลในการประกวด การแข่งขันการชิงโชค ค่าแสดงของนักแสดงสารธารณะ ค่าจ้างทำของ
                            ค่าโฆษณา ค่าเช่า ค่าขนส่ง ค่าบริการ ค่าเบี้ยประกันวินาศภัย ฯลฯ
                        </span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style=" border-bottom: 2px solid black;">
                        <td style="border-bottom: 2px solid black;border-top: 0; height: 20px; padding-right: 10px;">
                            <span class="indentspan">6. &nbsp;อื่นๆ ระบุ&nbsp;</span>
                            <span class="dotted incomeOther" style="word-wrap: break-word;width:310px;">
                                <?= isset($supplier['note']) ? $supplier['note'] : '' ?>
                                </span>

                        </td>
                        <td>
                            <?= !empty($money['Check_tax']) ? $money['Check_tax'] : '' ?>
                        </td>
                        <td>
                            <span
                                class="digits"><?= !empty($money['baht']) ? number_format($money['baht'], 2) : '' ?></span>
                        </td>
                        <td class="digits">
                            <span><?= !empty($money['wht']) ? number_format($money['wht'], 2) : '' ?></span>
                        </td>
                    </tr>
                    <tr style="border-top: 2px solid black;">
                        <td class="no-bottom" colspan="2" style="text-align: right;">
                            รวมเงินที่จ่ายและภาษีที่หักนำส่ง (บาท)
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-top: 2px solid;padding-right:5px;"
                            class="digits">
                            <?= !empty($money['baht']) ? number_format($money['baht'], 2) : '' ?>
                        </td>
                        <td style=" border-bottom: 1px solid; border-top: 2px solid;padding-right:5px;" class="digits">
                            <?= !empty($money['wht']) ? number_format($money['wht'], 2) : '' ?>
                        </td>
                    </tr>

                    <tr style="border-bottom: 1px solid;"> <!--display: inline-block;-->
                        <td style="font-size: 14px; padding-top: 5px; " colspan="4">
                            <span style=""><label
                                    style="font-weight: bold">รวมเงินภาษีที่หักนำส่ง</label> <i>(ตัวอักษร)</i>
                                <span
                                    class="textbaht"><?= !empty($money['wht']) ? FormatBaht($money['wht'], 2) : '' ?></span></span>
                        </td>

                    </tr>
                    </tbody>
                </table>

                <div style="height:2px;"></div>

                <div id="spend" class="pdf-margin"
                     style="border: 1px solid #000; margin-bottom: 2px; padding-top: 2px; border-radius: 5px;">
                    <span style="font-weight: bold; margin-left: 5px;">เงินที่จ่ายเข้า</span>
                    <span>กองทุนสำรองเลี้ยงชีพ ใบอนุญาตเลขที่_______จำนวนเงิน_________บาท กองทุนประกันสังคม_________บาท</span><br/>
                    <table style="width: 100%; border-top: 1px solid #000;">
                        <tr>
                            <td style="border-width: 0px; padding-right: 0px;">
                                <span style="font-weight: bold; margin-left: 2px;">ผู้จ่ายเงิน</span>
                            </td>
                            <td style="border-width: 0px; padding-left: 0px;">
                                <input style="margin-top:2px;" type="checkbox"
                                       class="canChk" <?= $company['PayerType'] == 1 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span
                                    style="vertical-align: text-bottom;line-height: 17px;"> หักภาษี ณ ที่จ่าย</span>
                            </td>
                            <td style="border-width: 0px;">
                                <input style="margin-top:2px;" type="checkbox"
                                       class="canChk"<?= $company['PayerType'] == 2 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span
                                    style="vertical-align: text-bottom;line-height: 17px;"> ออกภาษีให้ตลอดไป</span>
                            </td>
                            <td style="border-width: 0px; padding-left: 0px;">
                                <input style="margin-top:2px;" type="checkbox"
                                       class="canChk"<?= $company['PayerType'] == 3 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span
                                    style="vertical-align: text-bottom;line-height: 17px;"> ออกภาษีให้ครั้งเดียว</span>
                            </td>
                            <td style="border-width: 0px;">
                                <input style="margin-top:2px;" type="checkbox"
                                       class="canChk" <?= $company['PayerType'] == 4 ? 'checked=&#39;checked&#39;' : '' ?>/>
                                <span
                                    style="vertical-align: text-bottom;line-height: 17px;"> อื่นๆ (ระบุ)...........................</span>
                            </td>
                    </table>
                </div>

                <div class="warning-div pdf-margin  right-signa"
                     style="margin-top: 0; width: 31.5%; height: 130px; float: left; padding-left: 5px; padding-right: 5px;  border-radius: 5px;">
                    <div style="margin-top: 3px;word-wrap:break-word;max-width:219px;">
                        <span style="font-weight:bold;">หมายเหตุ</span> :
                        ให้สามารถอ้างหรือสอบยันกันได้ระหว่างลำดับที่ตามหนังสือรับรองฯกับแบบยื่นรายการหักภาษี ณ
                        ที่จ่าย <br/>
                    </div>
                    <div style="margin-top: 3px;word-wrap:break-word;max-width:219px;">
                        <span style="font-weight:bold;">คำเตือน</span> : ผู้มีหน้าที่ออกหนังสือรับรองการหักภาษี ณ
                        ที่จ่าย ฝ่าฝืนไม่ปฏิบัติตามมาตรา 50 ทวิ แห่งประมวลรัษฎากรต้องรับโทษทางอาญาตาม<br/>มาตรา 35
                        แห่งประมวลรัษฏากรด้วย <br/>
                    </div>

                </div>
                <div id="spend" class="pdf-margin right-signa"
                     style="border: 1px solid #000; border-radius: 5px; margin-bottom: 2px; width: 66%; float: right; height: 130px; ">
                    <div style="text-align: left; width: 60%; position: absolute;margin-top: 3px; padding-left: 5px;">
                        ขอรับรองว่า
                        ข้อความและตัวเลขดังกล่าวข้างต้นถูกต้องตรงกับความจริงทุกประการ
                    </div>
                    <div id="license" style="padding: 0 5px 0 5px;min-height:80px; ">
                        <div style="float:right; margin-top: -50px;">
                            <div id="advanced" class="circle">
                                <div class="text-circle">ประทับตรา<br/>นิติบุคคล</div>
                            </div>
                        </div>
                        <div style="position:absolute;text-align:left;">
                            <span class="date-sig" style="position: relative;left: 50px;top: 50px;">(<i>วัน เดือน ปี ที่ออกหนังสือรับรอง</i>)</span>
                        </div>

                        <div style="">

                            <div
                                style=" position: relative;left: 5px; margin-top: 60px; width: 60%; text-align: center">

                                ลงชื่อ&nbsp;_____________________________ผู้จ่ายเงิน
                            </div>
                            <div style="width:60%; margin-top: 5px; text-align: center">
                                <div style="margin-bottom: -10px;font-size: 16px;">
                                    <span><?= !empty($money['Date_issue']) ? $money['Date_issue'] : '' ?></span>
                                </div>
                                _____________________________
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <section class="permanent-footer">
            <div class="row">
                <div style="display:none;color:#676767;" class="pull-left invoicepaging">
                    <label class="title-label"></label> <label class="fill info-page"></label>
                </div>
                <div id="pagecopy" class="pull-left"
                     style="color: #676767; font-size: 10px; margin-top: 2px; margin-left: 2px;">
                    ฉบับที่ 1 สำหรับผู้ถูกหักภาษี ณ ที่จ่าย / ฉบับที่ 2 สำหรับผู้หักภาษี ณ ที่จ่าย
                </div>
                <div class="pull-right" style="color: #676767; font-size: 14px;">
                </div>
            </div>
        </section>
    </div>
</div>

<div class="btntop no-print">
    <?= Html::a('พิมพ์', ['index'], ['class' => 'button glow button-primary', 'onclick' => 'window.print()']) ?>
</div>
</body>
</html>
