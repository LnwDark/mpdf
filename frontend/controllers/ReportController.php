<?php

namespace frontend\controllers;

use kartik\mpdf\Pdf;
use yii\helpers\Url;
use mPDF;

class ReportController extends \yii\web\Controller
{

    public function actionReceipt()
    {
        $page = 1;

        $content = $this->renderPartial('index', [
            'page' => $page
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            'marginTop' => 10,
            'marginLeft' => 15,
            'marginRight' => 10,

            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
            // any css to be embedded if required
            'cssInline' => 'body{ font-family:"garuda", "sans-serif";font-size:12px;} h4{font-size:22px;}',
            // set mPDF properties on the fly
            'options' => [
                'title' => 'ใบกำกับภาษี / ใบเสร็จรับเงิน',

                'autoScriptToLang' => true,
            ],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader' => ['{GENO}PA'],
                //'SetFooter' => ['เราใช้งานระบบ{PAGENO}'],
            ]

        ]);


        // return the Pdfreport output as per the destination setting
        return $pdf->render();
    }

    public function actionTest()
    {
        $masterArray = [
            'no' => '',
            'date' => '12/01/2545',
            'd_invoice' => '2016111601',
            'book' => 'iv',
            'num_p' => '2016072101',
            'name_company' => 'ขายเงินสดให้บริษัท เจแมกซ์ไทย จำกัด',
            'tax_id' => '105559055912',
            'branch' => 'สาขาใหญ่',
            'price' => '1000',
            'vat' => '700',
        ];

        for ($i = 0; $i < 201; $i++) {
            $masterArray['no'] = $i + 1;
            $data[] = $masterArray;
        }
        return $data;


// echo count($data);
//        echo '<pre>';
//        var_dump($data);
        //return ;
    }


    public function actionTaxBuy()
    {
        $company = [
            'name' => 'บริษัท เนกเจนเซฟ จำกัด',
            'address' => '1558/41 ถนน บางนา-ตราด เขตบางนา กรุงเทพมหานคร10260',
            'tax_id' => '0105556004705',
            'branch' => '2',
            'month' => 'พฤศจิกายน ',
            'year' => '2559'
        ];

        $header = $this->renderPartial('header', [
            'company' => $company
        ]);
        $mpdf = new \mPDF('th', 'A4', '0','', $left = '5', $right = '5', $top = '45', '5');
        $rawData = $this->actionTest();
        $totalrow = count($rawData);
        $pagesize = 50;
        $sum = $totalrow / $pagesize;
        for ($i = 0; $i < $sum; $i++) {
            $page = $i + 1;
            //  $page =2;
            $start = $page * $pagesize - $pagesize;
            //  exit;
            $end = $page * $pagesize - 1;
            $lastindex = count($rawData) - 1;
            if ($end > $lastindex) {
                $end = $lastindex;
            }
            /*  echo $end;
              exit;*/

            $content = $this->renderPartial('tax_buy', [
                'model' => $rawData,
                'pagesize' => $pagesize,
                'pageNo' => $page,
                'start' => $start,
                'end' => $end,
            ]);

            $SumPrice = 0;
            $SumVat = 0;
            for ($a = $start; $a <= $end; $a++) {
                $SumPrice += $rawData[$a]['price'];
                $SumVat += $rawData[$a]['vat'];
            }
            $TotalPrice = 0;
            $TotalVat = 0;
            foreach ($rawData as $value) {
                $TotalPrice += $value['price'];
                $TotalVat += $value['vat'];
            }
            $footer = $this->renderPartial('footer', [
                'SumPrice' => $SumPrice,
                'SumVat' => $SumVat,
                'TotalPrice' => $TotalPrice,
                'TotalVat' => $TotalVat
            ]);
            $mpdf->SetHTMLHeader($header);
            $mpdf->AddPage();
            $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/mpdf/mpdf.css'));
            $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
            $mpdf->WriteHTML($content, 2);
            $mpdf->SetFooter($footer);
        }
        return $mpdf->Output();

    }

    public function actionNew()
    {
        $page=1;
        $footer=$this->renderPartial('_Invoicefooter');
        $content = $this->renderPartial('invoice',[
            'page' => $page
        ]);
        $mpdf = new \mPDF('th', 'A4','0','thsaraban', $left = '10', $right = '5', $top = '5', '30');
        $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/mpdf/bill.css'));
        $mpdf->SetHTMLFooter($footer);
        $mpdf->SetTitle('ใบเสร็จรับเงิน');
        $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
        $mpdf->WriteHTML($content,2);
        $mpdf->Output();
    }

    public function actionBilling()
    {
        $page=1;
        $footer=$this->renderPartial('billng/_billnfooter');
        $content = $this->renderPartial('billng/billnbody',[
            'page' => $page
        ]);
        $mpdf = new \mPDF('th', 'A4','0','thsaraban', $left = '10', $right = '5', $top = '5', '30');
        $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/mpdf/invoice.css'));
        $mpdf->SetHTMLFooter($footer);
        $mpdf->SetTitle('ใบเสร็จรับเงิน');
        $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
        $mpdf->WriteHTML($content,2);
        $mpdf->Output();
    }
    public function actionFont()
    {
//        $content=$this->renderPartial('font');
//       $mpdf =new mPDF('th','A4','');
//        $mpdf->SetFont('THSarabunNew');
//        $mpdf->WriteHTML($content);
//        $mpdf->Output();
        return $this->render('font');
    }
    public function actionAutoRefresh()
    {
        return $this->render('certificate-refresh', ['time' => date('H:i:s')]);
    }
}
