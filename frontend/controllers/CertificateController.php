<?php

namespace frontend\controllers;

class CertificateController extends \yii\web\Controller
{


public function actionCerCate()
{
    //ใบสำคัญรับ
    $content =$this->renderPartial('index');
    $header=$this->renderPartial('headercer-cate');
    $footer=$this->renderPartial('foottercer-cate');
    $mpdf = new \mPDF('th', 'A4','0','thsaraban', $left = '15', $right = '5', $top = '60', '20');
    $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/certificate.css'));
    $mpdf->SetHTMLHeader($header);
    $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
    $mpdf->WriteHTML($content,2);
    $mpdf->SetHTMLFooter($footer);
    $mpdf->Output();
}

public function actionMake()
{

    //รายงาน กระดาษทำการ
    $header=$this->renderPartial('headermake');
    $content =$this->renderPartial('make');
    $mpdf = new \mPDF('th', 'A4','0','', $left = '20', $right = '10', $top = '40', '20');
    $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/makes.css'));
    $mpdf->SetTitle('รายงาน');
    $mpdf->SetHTMLHeader($header);
    $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
    $mpdf->WriteHTML($content,2);
    $mpdf->Output();
}
public function actionBudget()
{
    //รายงาน งบทดลอง
    $header=$this->renderPartial('headerbudget');
    $content =$this->renderPartial('budget');
    $mpdf = new \mPDF('th', 'A4','0','', $left = '20', $right = '10', $top = '45', '10');
    $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/makes.css'));
    $mpdf->SetTitle('รายงาน');
    $mpdf->SetHTMLHeader($header);
    $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
    $mpdf->WriteHTML($content,2);
    $mpdf->Output();
}
public function actionBalance()
{
    //รายงานงบดุล
    $content =$this->renderPartial('balance');
    $mpdf = new \mPDF('th', 'A4','0','', $left = '15', $right = '10', $top = '20', '10');
    $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/makes.css'));
    $mpdf->SetTitle('รายงานงบดุล');
    $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
    $mpdf->WriteHTML($content,2);
    $mpdf->Output();
}
public function actionStatements()
{
    //งบกำไรขาดทุน
    $content =$this->renderPartial('statements');
    $mpdf = new \mPDF('th', 'A4','0','', $left = '15', $right = '10', $top = '20', '10');
    $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/makes.css'));
    $mpdf->SetTitle('รายงานงบกำไรขาดทุน');
    $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
    $mpdf->WriteHTML($content,2);
    $mpdf->Output();
}
    public function actionAccount()
    {
        //รายงานบัญชี
        $content =$this->renderPartial('account');
        $mpdf = new \mPDF('th', 'A4','0','', $left = '5', $right = '10', $top = '15', '10');
        $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/makes.css'));
        $mpdf->SetTitle('รายงานบัญชี');
        $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
        $mpdf->WriteHTML($content,2);
        $mpdf->Output();
    }

    public function actionReportCard()
    {
        $content =$this->renderPartial('cardwork');
        $mpdf = new \mPDF('th', 'A4','0','', $left = '15', $right = '10', $top = '15', '10');
        $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/css/cardwork.css'));
        $mpdf->SetTitle('ใบตรวจรับงาน');
        $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
        $mpdf->WriteHTML($content,2);
        $mpdf->Output();

    }

}
