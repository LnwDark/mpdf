<?php

namespace frontend\controllers;

use mikehaertl\pdftk\Pdf;
use yii\helpers\Url;



class PdfreportController extends \yii\web\Controller
{
    // แปลงบัตร ประชาชน
    protected function CitizenID($citizen_id)
    {

        if (strlen($citizen_id) == 13) {
            return substr($citizen_id, 0, 1) . ' ' . substr($citizen_id, 1, 4) .
            ' ' . substr($citizen_id, 5, 5) . ' ' . substr($citizen_id, 10, 2) .
            ' ' . substr($citizen_id, 12);
        }
        return $citizen_id;
    }

    protected function TaxNumber($tax)
    {

        if (strlen($tax) == 10) {
            return substr($tax, 0, 1) . ' ' . substr($tax, 1, 4) . ' ' . substr($tax, 5, 4) . ' ' . substr($tax, 9);
        }

        return $tax;
    }

    //แปลงตัวเลขเป็น ตัวอักษร
    protected function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000) {
            $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while ($number > 0) {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    protected function FormatBaht($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".", "");
        $pt = strpos($amount_number, ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .= $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function actionIndex()
    {
        $company = [
            'name' => 'บริษัท เนกเจน แอนด์ เนทออพ จำกัด',
            "address" => "90/12 เพล็กซ์ บางนา ม.15 ต.บางแก้ว อ.บางพลี จ.สมุทรปราการ 10540",
            "sub_district" => "",
            "district" => "",
            "province" => "",
            "postal_code" => "",
            "tax_id" => "1234567890",
            "tax_number" => "1234467890",
            "country" => "Thailand",
        ];
        $supplier = [
            "id" => 57,
            "name" => "นายสุริชัย โพธิ์ประโคน ",
            "tax_id" => "1310800155250",
            'tax_number' => "1234467890",
            "address" => "24 หมู2 ต.บึงเจรญ อ.บ้านกรวด จ.บุรีรัมย์",
            "branch" => "",
            "note" => "",
            "company_id" => "1",
            "created_at" => "2016-10-07",
        ];
        $formatter = \Yii::$app->formatter;
        $Date = $formatter->asDate($supplier['created_at'], 'short');
        $wht = 450;
        $baht = 15000;
        $type = 1;
        $PayType = 1;
        $date_pay = date('d');
        $month_pay = ' ' . '     ' . date('m');
        $year_pay = date('Y') + 543;
        $data = [
            //'book_no' => date('ym'),
            'name1' => $company['name'],
            'add1' => $company['address'],
            'id1' => strlen($company['tax_id']) == 13 ? $this->CitizenID($company['tax_id']) : '',
            'tin1' => strlen($company['tax_id']) == 10 ? $this->TaxNumber($company['tax_id']) : '',
            'id1_2' => strlen($supplier['tax_id']) == 13 ? $this->CitizenID($supplier['tax_id']) : '',
            'tin1_2' => strlen($supplier['tax_id']) == 10 ? $this->TaxNumber($supplier['tax_id']) : '',
            'name2' => $supplier['name'],
            'add2' => $supplier['address'],
            'run_no' => date('ym') . $supplier['id'],
            'chk1' => $type == 1 ? 'Yes' : 'No',
            'chk2' => $type == 2 ? 'Yes' : 'No',
            'chk3' => $type == 3 ? 'Yes' : 'No',
            'chk4' => $type == 4 ? 'Yes' : 'No',
            'chk5' => $type == 5 ? 'Yes' : 'No',
            'chk6' => $type == 6 ? 'Yes' : 'No',
            'chk7' => $type == 7 ? 'Yes' : 'No',
            'spec3' => '',

            'date14.1' => $Date,
            'pay1.13.1' => $baht,
            'tax1.13.1' => $wht,

            'pay1.14' => $baht,
            'tax1.14' => $wht,
            'total' => $this->FormatBaht($wht),
            'chk8' => $PayType == 1 ? 'Yes' : 'No',
            'chk9' => $PayType == 2 ? 'Yes' : 'No',
            'chk10' => $PayType == 3 ? 'Yes' : 'No',
            'chk11' => $PayType == 4 ? 'Yes' : 'No',
            'date_pay' => $date_pay,
            'month_pay' => $month_pay,
            'year_pay' => $year_pay,
        ];

        $pdf = new Pdf(Url::base() . '/Pdfreport/approve1.pdf');
        $pdf->cat(1);
        $pdf->fillForm($data);

        $pdf->needAppearances()->send();

//        if (!$Pdfreport->needAppearances()->saveAs(Url::base() . 'Pdfreport/' . rand() . date('d-m-Y') . '.Pdfreport')) {
//            $error = $Pdfreport->getError();
//        }
        return 'Generator ok';
    }


    public function actionTest()
    {
        $date = '2016-10-07';
        \Yii::$app->formatter->locale = 'th-TH';
        echo \Yii::$app->formatter->asDate($date, 'short');
        $tax_number = 1234567890;
        //return $this->Convert($num);
        return $this->TaxNumber($tax_number);

    }

    public function actionNew()
    {
//        $Pdfreport = new \Jurosh\PDFMerge\PDFMerger;
//        $Pdfreport->addPDF(\Yii::getAlias('@frontend/web/Pdfreport/approve1.Pdfreport'), 'all')
//            ->addPDF(\Yii::getAlias('@frontend/web/Pdfreport/approve1.Pdfreport'));
//        $Pdfreport->merge('file',\Yii::getAlias('@frontend/web/Pdfreport/new.Pdfreport'));
        //$mpdf =new Pdf();
//        return $mpdf->Output();
        return dirname(__FILE__);
    }

}
