<?php

namespace frontend\controllers;

use kartik\mpdf\Pdf;
use yii\helpers\Url;


class BillnController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $page = 1;
        $data = [
            'income_type' => '2',
            'logo' => 'ischool.png',
            'Receipt' => 'ใบกำกับภาษี / ใบเสร็จรับเงิน',
            'Code_Bill' => 'BILL123004',
            'Date_Bill' => date('d/m/Y'),
            'Seller_Bill' => 'บัญชี  ซื่อสัตย์',
            'Date_duly' => date('d/m/Y'),
            'name' => 'บริษัท เนกเจน แอนด์ เนทออพ จำกัด',
            'address' => '1558/41 ถ.บางนา-ตราด',
            'sub_district' => 'แขวงบางนา',
            'district' => 'เขตบางนา',
            'province' => 'กรุงเทพฯ',
            'postal_code' => '10260',
            'branch' => 'สำนักงานใหญ่',
            'tax_id' => '0105559127204',
            'telephone' => '02-101-4822',
            'Customer_name' => 'สมาคมผู้ปกครองและครูโรงเรียนถาวรานุกุล',
            'Customer_address' => '99/9 ม.2 ต.หนองปรือ อ.บางพลี จ.สมุทรปราการ 10260',
            'Customer_tax_id' => '099400042493',
            'Note' => 'โปรดจ่ายเช็คขีดคร่อมในนาม  บริษัท เนกเจน แอนด์ เนทออพ จำกัด หรือโอนเข้าบัญชี ธนาคารกสิกรไทย สาขาเซ็นทรัลบางนา
                     ชื่อบัญชี : บริษัท เนกเจน แอนด์ เนทออพ จำกัด
                     เลขที่บัญชี : 016-1-97701-2',
        ];
        $footer = $this->renderPartial('_billnfooter', [
            'data' => $data,
        ]);
        $content = $this->renderPartial('setpage', [
            'page' => $page,
            'data' => $data,

        ]);
        $mpdf = new \mPDF('th', 'A4', '0', '', $left = '10', $right = '5', $top = '5', '30');
        $stylesheet = file_get_contents(\Yii::getAlias('@frontend/web/mpdf/bill.css'));
        $mpdf->SetHTMLFooter($footer);
        if ($data['income_type'] == 0) {
            $mpdf->SetTitle('ใบเสนอราคา');
        } elseif ($data['income_type'] == 1) {
            $mpdf->SetTitle('ใบวางบิล');
        } elseif ($data['income_type'] == 2) {
            $mpdf->SetTitle('ใบเสร็จรับเงิน');
        }
        $mpdf->WriteHTML($stylesheet, 1); //เพิ่ม css
        $mpdf->WriteHTML($content, 2);
        $mpdf->Output();
    }

    public function actionFont()
    {
//        $content=$this->renderPartial('font');
//       $mpdf =new mPDF('th','A4','');
//        $mpdf->SetFont('THSarabunNew');
//        $mpdf->WriteHTML($content);
//        $mpdf->Output();
        return $this->render('font');
    }
}
